#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "armexec.h"

struct vm {
    struct elf  *elf;
    char        *heap;
    uint32_t    curr_heap;
};

volatile uint32_t vm_mem_watch = 0xffffffff;
volatile int vm_mem_watch_type = -1;

static __thread char *vm_stack = NULL;
static pthread_mutex_t malloc_lock = PTHREAD_MUTEX_INITIALIZER;

uint32_t
vm_malloc(struct vm *vm, uint32_t len, void **ph)
{
    uint32_t addr;
    uint32_t *plen;
    void *p;

    if ((len & 3) != 0) {
        len += 4;
        len &= ~3;
    }

    pthread_mutex_lock(&malloc_lock);

    if ((vm->curr_heap + len + 4) > VM_HEAP_END) {
        PANIC("heap overflow\n");
    }

    p = vm->heap + (vm->curr_heap - VM_HEAP_START);
    plen = (uint32_t *) p;
    *plen = len;
    addr = vm->curr_heap + 4;
    vm->curr_heap += len + 4;

    pthread_mutex_unlock(&malloc_lock);

    if (ph)
        *ph = p + 4;

    return addr;
}

uint32_t
vm_malloc_string(struct vm *vm, char *s)
{
    uint32_t data;
    void *p;

    data = vm_malloc(vm, strlen(s) + 1, &p);
    strcpy(p, s);

    return data;
}

void *
vm_phy_addr(struct vm *vm, uint32_t addr)
{
    void *p = NULL;

    if (!addr)
        return NULL;

    /* Check stack and heap first */

    if (addr >= VM_STACK_START && addr < VM_STACK_END) {
        /* stack is created on demand */
        if (!vm_stack)
            vm_stack = calloc(1, VM_STACK_SIZE);

        return vm_stack + (addr - VM_STACK_START);
    }

    if (addr >= VM_HEAP_START && addr < VM_HEAP_END)
        return vm->heap + (addr - VM_HEAP_START);

    if (vm->elf)
        p = elf_phy_addr(vm->elf, addr);

    if (!p)
        PANIC("Invalid addr: %x\n", addr);

    return p;
}

void
vm_set_elf(struct vm *vm, struct elf *elf)
{
    vm->elf = elf;
}

struct vm *
vm_init(void)
{
    struct vm *vm;

    vm = calloc(1, sizeof(struct vm));
    vm->heap = calloc(1, VM_HEAP_SIZE);
    vm->curr_heap = VM_HEAP_START;

    return vm;
}
