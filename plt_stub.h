#ifndef _PLT_STUB_H
#define _PLT_STUB_H

#include <stdint.h>
#include <time.h>
#include <sys/time.h>

#define DCLR_STUB(NAME) \
uint32_t NAME ## _stub(struct armld *ld,\
                       uint32_t r0,\
                       uint32_t r1,\
                       uint32_t r2,\
                       uint32_t r3,\
                       uint32_t sp)

#define STUB_ENT(NAME)      {0, #NAME, NAME ## _stub}
#define STUB_ENT_DUM(NAME)  {0, #NAME, dummy_stub}

/* defined in plt_stub.c */
DCLR_STUB(dummy);


/* time is not compatible between 32 and 64 platform */
typedef uint32_t time32_t;

struct timeval32 {
    uint32_t    tv_sec;
    uint32_t    tv_usec;
};

struct timespec32 {
    time32_t    tv_sec;
    uint32_t    tv_nsec;
};

struct armld;

uint32_t
vsprintf_stub(int start,
              struct armld *ld,
              char *dst,
              char *fmt,
              uint32_t r0,
              uint32_t r1,
              uint32_t r2,
              uint32_t r3,
              uint32_t sp);

/* stdlib_stub */
DCLR_STUB(malloc);
DCLR_STUB(free);
DCLR_STUB(realloc);
DCLR_STUB(strtoul);
DCLR_STUB(srand48);
DCLR_STUB(bsd_signal);
DCLR_STUB(usleep);

/* string_stub */
DCLR_STUB(memset);
DCLR_STUB(memcpy);
DCLR_STUB(memmove);
DCLR_STUB(strcpy);
DCLR_STUB(strncpy);
DCLR_STUB(strlen);
DCLR_STUB(strdup);
DCLR_STUB(strcmp);
DCLR_STUB(strcasecmp);
DCLR_STUB(strncmp);
DCLR_STUB(strcat);
DCLR_STUB(strncat);
DCLR_STUB(strchr);
DCLR_STUB(memcmp);
DCLR_STUB(strstr);
DCLR_STUB(memchr);
DCLR_STUB(strncasecmp);
DCLR_STUB(strrchr);

/* sprintf stub */
DCLR_STUB(sprintf);
DCLR_STUB(printf);

/* socket stub */
DCLR_STUB(inet_addr);
DCLR_STUB(inet_ntoa);
DCLR_STUB(pipe);
DCLR_STUB(close);
DCLR_STUB(socket);
DCLR_STUB(fcntl);
DCLR_STUB(setsockopt);
DCLR_STUB(getsockopt);
DCLR_STUB(bind);
DCLR_STUB(listen);
DCLR_STUB(getsockname);
DCLR_STUB(write);
DCLR_STUB(select);
DCLR_STUB(read);
DCLR_STUB(sendto);
DCLR_STUB(send);
DCLR_STUB(ioctl);
DCLR_STUB(recv);
DCLR_STUB(shutdown);
DCLR_STUB(connect);
DCLR_STUB(getaddrinfo);
DCLR_STUB(recvfrom);
DCLR_STUB(accept);

/* time stub */
DCLR_STUB(time);
DCLR_STUB(gettimeofday);
DCLR_STUB(nanosleep);
DCLR_STUB(gmtime);

/* pthread stub */
DCLR_STUB(pthread_create);
DCLR_STUB(sem_init);
DCLR_STUB(pthread_mutex_init);
DCLR_STUB(pthread_mutex_destroy);
DCLR_STUB(pthread_self);
DCLR_STUB(pthread_detach);
DCLR_STUB(pthread_join);
DCLR_STUB(pthread_exit);
DCLR_STUB(sem_destroy);
DCLR_STUB(sem_wait);
DCLR_STUB(pthread_mutex_lock);
DCLR_STUB(pthread_mutex_unlock);
DCLR_STUB(sem_post);
DCLR_STUB(__errno);
DCLR_STUB(sched_yield);
DCLR_STUB(sem_getvalue);
DCLR_STUB(pthread_attr_setdetachstate);

/* mq */
DCLR_STUB(mq_open);
DCLR_STUB(mq_send);
DCLR_STUB(mq_close);
DCLR_STUB(mq_timedreceive);

#endif
