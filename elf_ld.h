#ifndef _ELF_LD_H
#define _ELF_LD_H

#include <stdint.h>
#include <stdio.h>

struct elf;
struct Elf32_Shdr;
struct vm;
struct armld;

typedef uint32_t (*elf_stub_func_t)(struct armld *ld,
                                    uint32_t r0,
                                    uint32_t r1,
                                    uint32_t r2,
                                    uint32_t r3,
                                    uint32_t sp);
typedef struct {
    uint32_t        addr;
    char            *name;
    elf_stub_func_t func;
} elf_stub_t;

typedef struct {
#define ELF_OBJ_T_DATA      0
#define ELF_OBJ_T_POINTER   1
    int             type;

    char            *name;
    void            *data;
    uint32_t        size;
} elf_obj_t;

struct elf *
elf_load(struct vm *, FILE *fp);

void *
elf_phy_addr(struct elf *elf, uint32_t addr);

elf_stub_t *
elf_lookup_stub(struct elf *elf, uint32_t addr);

void
elf_add_stubs(struct elf *elf, elf_stub_t *stubs, int cnt);

char *
elf_lookup_name(struct elf *elf, uint32_t addr);

#endif
