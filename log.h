#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdlib.h>

extern __thread uint32_t exec_curr_insn;

#define INFO_ON 1

#define PANIC(fmt, ...) do { \
    fprintf(stderr, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stderr, fmt, ##__VA_ARGS__); \
    abort(); \
} while (0)

#ifdef DEBUG_ON
#define DEBUG(fmt, ...) do { \
    fprintf(stdout, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define DEBUG(...) do {} while (0)
#endif

#ifdef INFO_ON
#define INFO(fmt, ...) do { \
    fprintf(stdout, "[%d]:[%x]: ", thread_id, exec_curr_insn); \
    fprintf(stdout, fmt, ##__VA_ARGS__); \
} while (0)
#else
#define INFO(...) do {} while (0)
#endif

#endif
