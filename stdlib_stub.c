#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>

#include "armexec.h"
#include "plt_stub.h"


DCLR_STUB(malloc)
{
    struct vm *vm = ld->vm;
    return vm_malloc(vm, r0, NULL);
}

DCLR_STUB(realloc)
{
    struct vm *vm = ld->vm;
    void *p;
    void *p2;
    uint32_t *size;
    uint32_t addr = vm_malloc(vm, r1, &p2);

    if (r0) {
        p = PA(r0);
        size = p;
        memcpy(p2, p, *(size - 1));
    }

    return addr;
}

DCLR_STUB(strtoul)
{
    struct vm *vm = ld->vm;
    char *s = PA(r0);
    char *end;
    int base = r2;
    unsigned long i;
    uint32_t ret;

    i = strtoul(s, &end, base);
    if (i == 0) {
        /* no digit at all */
        if (r1)
            SW(r1, r0);
        ret = 0;
    } else if (i == ULONG_MAX) {
        /* overflow */
        ret = 0xffffffff;
    } else {
        if (r1)
            SW(r1, r0 + (end - s));
        ret = i;
    }

    return ret;
}

DCLR_STUB(srand48)
{
    return rand();
}

DCLR_STUB(bsd_signal)
{
    signal(r0, SIG_IGN);
    return 0;
}

DCLR_STUB(usleep)
{
    usleep(r0);
    return 0;
}
