#ifndef _ARM_EXEC_H
#define _ARM_EXEC_H

#include <stdint.h>
#include <stdio.h>

struct vm;
struct elf;

struct armld {
    struct vm       *vm;
    struct elf      *elf;
};

/* Defined in pthread_stub.c */
extern __thread int thread_id;

/* Defined in exec.c */
extern __thread uint32_t exec_curr_insn;

#define __breakpoint() do { \
    __asm__ volatile("int $0x03"); \
} while(0)

#include "log.h"
#include "vm.h"
#include "elf_ld.h"
#include "exec.h"

#endif
