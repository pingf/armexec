#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <mqueue.h>

#include "armexec.h"
#include "plt_stub.h"

/* mqd_t is int in all system.
 * timespec needs translation.
 * mq_attr needs translation.
 */

struct mq_attr32 {
    uint32_t    mq_flags;
    uint32_t    mq_maxmsg;
    uint32_t    mq_msgsize;
    uint32_t    mq_curmsgs;
};

DCLR_STUB(mq_open)
{
    struct vm *vm = ld->vm;
    char *name = PA(r0);
    struct mq_attr32 *attr32 = PA(r3);
    mqd_t mq;
    struct mq_attr attr;

    mq_unlink(name);

    /* Ingore the given param which has wrong mq_maxmsg num */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 6;
    attr.mq_msgsize = 16;
    attr.mq_curmsgs = 0;

    mq = mq_open(name,
                 O_RDWR | O_CREAT,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                 &attr);

    if (mq == -1) {
        perror("mq_open");
        INFO("warning: mq_open fail\n");
    }
    INFO("mq_open: %d\n", mq);

    return mq;
}

DCLR_STUB(mq_send)
{
    struct vm *vm = ld->vm;
    mqd_t mq = r0;
    char *p = PA(r1);
    int ret;

    ret = mq_send(mq, p, r2, r3);
    if (ret == -1) {
        perror("mq_send");
        INFO("warning: mq_send fail\n");
    }

    return ret;
}

DCLR_STUB(mq_close)
{
    return mq_close(r0);
}

DCLR_STUB(mq_timedreceive)
{
    /* r0 - mq
     * r1 - *msg
     * r2 - len
     * r3 - *prio / ignore
     * [sp] - timeout
     */
    struct vm *vm = ld->vm;
    mqd_t mq = r0;
    char *p = PA(r1);
    uint32_t data;
    struct timespec32 *tm32;
    struct timespec ts;
    int ret;

    data = LW(sp);
    tm32 = PA(data);
    ts.tv_sec = tm32->tv_sec;
    ts.tv_nsec = tm32->tv_nsec;

    ret = mq_timedreceive(mq, p, r2, NULL, &ts);
    if (ret == -1) {
        INFO("warning: mq_recv fail\n");
        perror("mq_recv");
    } else {
        INFO("mq_recv OK\n");
    }

    return ret;
}
