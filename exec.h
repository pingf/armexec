#ifndef _EXEC_H
#define _EXEC_H

#include <stdint.h>

struct armld;

typedef struct {
    char        *name;
    uint32_t    entry;
    uint32_t    lr;
} exec_frame_t;

int
arm_exec(struct armld *ld,
         uint32_t start,
         uint32_t halt,
         uint32_t *args,
         int cnt);

void
exec_load_registers(uint32_t core[16], int cpsr[4]);

exec_frame_t *
exec_get_backtrace(int *depth);

void
exec_set_r1(uint32_t r);

void
exec_set_breakpoint(uint32_t addr);

#endif
