#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <time.h>
#include <sys/time.h>

#include "armexec.h"
#include "plt_stub.h"


DCLR_STUB(time)
{
    struct vm *vm = ld->vm;
    time_t t = time(0);
    time32_t t32 = t;

    if (r0)
        memcpy(PA(r0), &t32, 4);

    return (uint32_t)t32;
}

DCLR_STUB(gettimeofday)
{
    struct vm *vm = ld->vm;
    struct timeval tv;
    struct timeval32 tv32;

    gettimeofday(&tv, NULL);

    tv32.tv_sec = tv.tv_sec;
    tv32.tv_usec = tv.tv_usec;

    memcpy(PA(r0), &tv32, sizeof(tv32));

    return 0;
}

DCLR_STUB(nanosleep)
{
    struct vm *vm = ld->vm;
    struct timespec32 *tm32 = PA(r0);
    struct timespec ts;

    ts.tv_sec = tm32->tv_sec;
    ts.tv_nsec = tm32->tv_nsec;

    nanosleep(&ts, NULL);

    return 0;
}

DCLR_STUB(gmtime)
{
    struct vm *vm = ld->vm;
    struct tm *tm, *tmr;
    time32_t *t32;
    time_t t;
    uint32_t data;

    t32 = PA(r0);
    t = *t32;
    tm = gmtime(&t);

    data = vm_malloc(vm, sizeof(struct tm), (void **) &tmr);
    memcpy(tmr, tm, sizeof(struct tm));

    return data;
}
